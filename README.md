# Pandas Learning For Wordpress Integration

Created a script to convert custom datasets to WordPress (woocommerce) compatible import files

Usage: python ScanExcelSchema

Using the mappings.csv and the xlrd library, the script reads the sample input files (all excel sheets) within a given library. The mappings indicate what fields the source data should map to in the WooCommerce import file that is created after the process is run.

metas.txt lists the fields which should be listed as meta_fields in WooCommerce.

attributes.txt helpt to create custom variable attributes to be used for product variants.

# TODO: Read user library at script runtime
# TODO: Remove constants from script and define using config files