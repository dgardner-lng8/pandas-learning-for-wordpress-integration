#/bin/bash

# read excel file. 
# Extract common fields
# Build attribute list
# Build Meta Fields

# read and parse all excel files in directory supplied
import xlrd 
import pandas as pd
import numpy as np
import os

def parseStandard(dataFrame):
        dataFrame.head()

def addMetaFields(inputDF):
        #read fields from meta file
        #use fields to build against input DF
        fields = open("metas.txt").read().splitlines()
        print(fields)

        for meta in fields:
                source = meta.strip()
                label = str.lower(source).replace('depth','length').replace(' ','_')
                try:
                        inputDF['Meta: '+label] = np.where(inputDF['Type']=='variation', inputDF[source], '')
                except:
                        print("This meta doesn't exit: "+source)

def addAttributes(inputDF):
         #read fields from meta file
        #use fields to build against input DF
        fields = open("attributes.txt").read().splitlines()
        print(fields)
        attrNum = 1
        skip = False
        for attr in fields:
                source = attr.strip()
                if skip and "Variation" in source:
                        continue

                if "Variation" in source:
                        inputDF["Attribute "+str(attrNum) +" Name"] = inputDF['Variation Name']
                        inputDF["Attribute "+str(attrNum) +" Value(s)"] = inputDF['Variation Value']
                        inputDF["Attribute "+str(attrNum) +" Visible"] = np.where(inputDF['Type']=='variable',1,None)
                        inputDF["Attribute "+str(attrNum) +" Global"] = 0
                        skip = True
                        
                else:         
                        inputDF["Attribute "+str(attrNum) +" Name"] = np.where(inputDF['Type']=='variable', source, '')
                        inputDF["Attribute "+str(attrNum) +" Value(s)"] = np.where(inputDF['Type']=='variable' , inputDF[source], '')
                
                        inputDF["Attribute "+str(attrNum) +" Visible"] = np.where(inputDF['Type']=='variable',1,None)
                        inputDF["Attribute "+str(attrNum) +" Global"] = np.where(inputDF['Type']=='variable',1,None)

                attrNum = attrNum + 1

def customBulletParser(textBlock):
        html_header = '<ul class="prod-quick-desc">'
        html_footer = '</ul>'
        newTextBlock = '<li>'+textBlock+'</li>'
        textList = newTextBlock.splitlines()
        newTextBlock = '</li>\n<li>'.join(textList)
        return html_header + '\n'+newTextBlock+'\n'+html_footer

def customCategoryParser(category):
        cat_output = []
        for categ in category.split(','):
                cat_val = ''
                if '>' in categ:
                        for cat in categ.split('>'):
                                if cat_val:
                                        cat_val = cat_val+', '+ cat_val+ '>'+ cat
                                else:
                                        cat_val = cat

                        cat_output.append(cat_val)
                else:
                        cat_output.append(categ)
        
        return ', '.join(cat_output)
                              

def parseParentStringFields(parentDF):
        # prepend html to the bullets
        parentDF['Bullets'] = parentDF.apply(lambda x:customBulletParser(x['Bullets']),axis=1)
        parentDF['Category'] = parentDF.apply(lambda x:customCategoryParser(x['Category']),axis=1)

def setParentIds(inputDF):
        inputDF['Parent'] = np.where(inputDF['Type'] == 'variation','id:'+(inputDF['ID'] - inputDF['Position']).map(str),'')
        inputDF['Model/Sku #'] = np.where(inputDF['Type'] == 'variable', (inputDF['Model/Sku #']+'-'+inputDF['ID'].map(str)),inputDF['Model/Sku #'])


def setVariantOrder(parentDF, inputDF):
        for idx, row in parentDF.iterrows():
                key = row["Item Name"]
                tempDF = inputDF.loc[inputDF["Item Name"].str.contains(key)]
                tempDF['Position'] = range(1, len(tempDF) + 1)

                inputDF.loc[inputDF['Item Name'].isin(tempDF['Item Name']), ['Position','Parent']] = tempDF.loc[tempDF['Item Name'].isin(inputDF['Item Name']),['Position','Parent']].values


def setParentVariantFields(parentDF, inputDF):
        #print(inputDF.head())
        inputDF["Variation Value"] = inputDF["Variation Value"].str.replace(',',' *')
        for idx, row in parentDF.iterrows():
                key = row["Item Name"]
                #fieldList = str(row["Variation Name"]).split(',')
                fieldList = ["Variation Value"]
                fieldsAsString = ','.join(fieldList)
                cols = fieldList.copy()
                cols.append("Item Name")
                cols.append("Variation Name")

                tempDF = inputDF.loc[inputDF["Item Name"].str.contains(key),cols]
                tempDF[cols] = tempDF[cols].astype(str)

                #tempDF['Rack Units'] = tempDF[['Item Name','Rack Units']].groupby('Item Name')['Rack Units'].transform(lambda x: ','.join(x))
                tempDF[fieldsAsString] = tempDF[cols].groupby('Item Name')[fieldsAsString].transform(lambda x: ','.join(x))
                tempDF = tempDF.drop_duplicates()
                
                for field in fieldList:
                        parentDF.loc[idx,field] = str(tempDF[field].iloc[0])

def mapFields(inputDF):
        #drop unused columns
        dropList = ['Variation Name','Variation Value','UPC','Color','Rack Units','Load Capacity','Package Length','Package Width','Package Height','Package Weight','Bullets']
        inputDF.drop(columns = dropList,inplace=True)

        # map the remaining columns
        # load mappings dataframe
        mappings = pd.read_csv(os.getcwd()+'\mappings.csv')
        print(mappings)
        finalDF = pd.DataFrame(columns=mappings['Destination'].values.tolist())
        
        for idx, row in mappings.iterrows():
                destination = str(row['Destination'])
                source = str(row['Source']).replace('nan','')
                default = str(row['Default']).replace('nan','')

                if source:
                        finalDF[destination] = inputDF[source]
                        inputDF.drop(columns = [source],inplace=True)
                elif default:
                        finalDF[destination] = default
                
                
        attrCols = [col for col in inputDF.columns if 'Attribute ' in col]
        metaCols = [col for col in inputDF.columns if 'Meta: ' in col]

        for attrCol in attrCols:
                finalDF[attrCol] = inputDF[attrCol]
        
        for metaCol in metaCols:
                finalDF[metaCol] = inputDF[metaCol]

        print(finalDF.head())
        return finalDF

def parseFile(path):
    # Load spreadsheet
    xl = pd.ExcelFile(path)

    # Load first sheet into a DataFrame
    inputDF = xl.parse('Sheet1')
    inputDF['Position'] = 0
    inputDF["Tax Class"] = ""
    inputDF["Parent"] = ""
    inputDF["reviews"] = 0
    uniqueDF = inputDF.copy()
    
    uniqueDF.drop_duplicates(subset='Item Name', keep="first", inplace=True)
    uniqueDF["reviews"]= 1
    setParentVariantFields(uniqueDF,inputDF)
    setVariantOrder(uniqueDF, inputDF)
    parseParentStringFields(uniqueDF)

    inputDF["Type"] = "variation"
    inputDF["Tax Class"] = "parent"
    
    uniqueDF["Type"] = "variable"
    discardVarFields = ['Category']
    discardMainFields = ['Weight (lbs)','Length (in)','Width (in)','Height (in)']

    for field in discardVarFields:
            inputDF[field] = ''

        
    unionDF = pd.concat([uniqueDF,inputDF])
    unionDF['Short Description'] = unionDF['Product Description']+ '\n\nAdditional features:\n\n' +unionDF['Bullets']
    addAttributes(unionDF)
    addMetaFields(unionDF)

    unionDF.sort_values(['Item Name','Position'],inplace=True)
    unionDF.insert(0,'ID',range(2000,2000 + len(unionDF)))
    setParentIds(unionDF)

    unionDF['Item Name'] = np.where(unionDF['Type']=='variation', unionDF['Item Name'] + ' - '+ unionDF['Variation Value'], unionDF['Item Name'])
    unionDF = mapFields(unionDF)
    unionDF.to_csv('out.csv',index=False)    

# change standardDir value
standardDir = "/Static files/Standard-Files"
for root, dirs, files in os.walk(standardDir):
    for file in files:
        if file.endswith(".xlsx") or  file.endswith(".xls"):
            s = os.path.join(root, file)
            print("Loading file: "+s)
            parseFile(s)

